﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class Program
    {
        static Random random = new Random();
        static Stopwatch sw = new Stopwatch();
        static void Main(string[] args)
        {
            //16,777,216 2^24
            int[] test = new int[1147483591];
            for (int i = 0; i < test.Length; i++)
                test[i] = random.Next(2147483591);//2^15

//            int[] distarr = new int[50000];
//            int[] times = new int[1000];
//            int timesindex = 0;
//            int increment = 500;
//            for (int i = 0; i < 50000; i += increment)
//            {
//                for (int k = 0; k < 4; k++)
//                {
//                    for (int j = 0; j < distarr.Length; j++)
//                        distarr[j] = random.Next(i);
//                    sw.Restart();
//                    sort(distarr);
//                    sw.Stop();
//                    times[timesindex] += (int) sw.ElapsedMilliseconds;
//                }
//
//                times[timesindex] /= 4;
//                timesindex++;
//                Console.WriteLine(i);
//            }
//            StringBuilder sb = new StringBuilder();
//            for (int i = 0; i < timesindex; i++)
//                sb.Append(i * increment + "," + times[i] + "\n");
//            File.WriteAllText("output.txt", sb.ToString());
//            return;
                int[] a = (int[]) test.Clone();
            int[] b = (int[]) test.Clone();
            for (int i = 0; i < 10; i++)
            {
                sw.Restart();
                sort(a);
                sw.Stop();
                Console.WriteLine("Me: " + sw.ElapsedMilliseconds);
                sw.Restart();
                Array.Sort(b);
                sw.Stop();
                Console.WriteLine("Default: " + sw.ElapsedMilliseconds);
                Console.WriteLine();
            }

            Console.Read();
        }
        static void sort(int[] arr)
        {
            Node[] nodes = new Node[256];
            int index;
            for (int i = 0; i < arr.Length; i++)
            {
                index = arr[i] >> 24;
                Node currNode = null;
                if (nodes[index] == null)
                    nodes[index] = new Node();
                currNode = nodes[index];
                currNode.occurrences++;

                index = (arr[i] >> 16) & 0xFF;
                if (currNode.children[index] == null)
                    currNode.children[index] = new Node();
                currNode = currNode.children[index];
                currNode.occurrences++;

                index = (arr[i] >> 8) & 0xFF;
                if (currNode.children[index] == null)
                    currNode.children[index] = new Node();
                currNode = currNode.children[index];
                currNode.occurrences++;

                index = arr[i] & 0xFF;
                if (currNode.children[index] == null)
                    currNode.children[index] = new Node();
                currNode = currNode.children[index];
                currNode.occurrences++;
            }

            index = 0;
            for (int i = 0; i < nodes.Length; i++)
            {
                if (nodes[i] == null) continue;
                int shiftedi = i << 24;
                for (int j = 0; j < nodes[i].children.Length; j++)
                {
                    if (nodes[i].children[j] == null) continue;
                    int shiftedj = j << 16;
                    for (int k = 0; k < nodes[i].children[j].children.Length; k++)
                    {
                        if (nodes[i].children[j].children[k] == null) continue;
                        int shiftedk = k << 8;
                        for (int l = 0; l < nodes[i].children[j].children[k].children.Length; l++)
                        {
                            if (nodes[i].children[j].children[k].children[l] == null) continue;
                            for (int m = 0; m < nodes[i].children[j].children[k].children[l].occurrences; m++)
                            {
                                arr[index++] = shiftedi + shiftedj + shiftedk + l;
                            }
                        }
                    }
                }
            }
        }
    }

    class Node
    {//make null array and declare when adding stuff - so we dont have 256 nullptrs for every value at the bottom
        public Node[] children = new Node[256];
        public int occurrences;
    }
}
